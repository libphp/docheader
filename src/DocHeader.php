<?php

/* Class to make header section of an html-page
 *
 *   Default value for doctype, title and charset
 *
 *   Links to css and javascript files can be added with 
 *   set_javascript(file) and set_css(file) 
 *   or add in stead of set, to add more files.
 *
 *   Other header properties can be set as well.
 *
 *   Header is printed to output, by calling display()
 */

namespace ougar\DocHeader;

class DocHeader{

  public $doctype;
  public $title;
	private $charset;
	private $javascript;
	private $css;
	public $icon;
	public $keywords;
	public $description;
	public $headertext;
	public $custom="";
	public $userdefined;

	public function __construct(array $properties=array()) {
	  $this->set_default();
		foreach ($properties as $key => $value) {
			if (!method_exists($this, "set_$key"))
			  throw new Exception("Unknown header property specified: $key");
      call_user_func(array($this, "set_$key"), $value);
		}
    #if (array_key_exists("doctype", $properties)) set_array_property("doctype", $properties, "doctypes");
    #if (array_key_exists("charset", $properties)) set_array_property("charset", $properties, "charsets");
		#if (array_key_exists("css", $properties)) {set_css($properties["css"]); unset($properties["css"]);};
		#if (array_key_exists("javascritp", $properties)) {set_javascript($properties["javascript"]); unset($properties["javascript"]);};
		#parent::__construct($properties);
	}

/*
	private set_array_property($name, $values, $arrayname) {
	  if (!array_key_exists($values[$name], $this->$array)) {
		  throw new Exception("Unknown $name specified");
		$a=$this->$array;
		$this->$name=$a[$values[$name]];
		unset($values[$name]);
	}
*/
  public function set_html5() {
    $this->set_doctype("html5");
    $this->set_charset("utf8_html5");
  }

	public function set_doctype($doctype) {
	  $doctypes=array(
	    "strict"        => '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">',
		  "transitional"  => '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">',
		  "frameset"      => '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">',
		  "xstrict"       => '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">',
      "xtransitional" => '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">',
		  "xframeset"     => '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">',
		  "xhtml11"       => '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">',
      "html5"         => '<!DOCTYPE html>');
		if (!array_key_exists($doctype, $doctypes))
		  throw new Exception("Unknown doctype \"$doctype\" specified");
		$this->doctype=$doctypes[$doctype];
	}

	public function set_charset($charset) {
    $charsets=array("utf8" => "UTF-8", "iso" => "ISO-8859-1");
    if ($charset=="utf8_html5") $this->charset="  <meta charset=\"utf-8\">\n";
    else if (!array_key_exists($charset, $charsets))
		  throw new Exception("Unknown charset \"$charset\" specified");
    else
		  $this->charset='  <meta http-equiv="Content-Type" content="text/html; charset='.$charsets[$charset]."\">\n";
	}

  public function add_mycss_table($style="table") {
    $this->add_css("http://ougar.dk/css/$style.css");
  }

	public function add_css($css) {
	  foreach ((array)$css as $entry) {
			$versiontime=@filemtime($entry);
  		$this->css.="  <link rel=\"stylesheet\" type=\"text/css\" href=\"$entry?$versiontime\">\n";
		}
	}
	public function add_javascript($java) {
	  foreach ((array)$java as $entry)
  		$this->javascript.='  <script type="text/javascript" src="'.$entry."\"></script>\n";
	}

  public function add_jquery() {
    $temp=$this->javascript;
    $this->set_javascript("http://www.ougar.dk/javascript/jquery-1.7.1.min.js");
    $this->javascript.=$temp;
  }

  public function add_jquery3() {
    $temp=$this->javascript;
    $this->set_javascript("http://www.ougar.dk/javascript/jquery-3.2.1.min.js");
    $this->javascript.=$temp;
  }

	public function set_css($css) {
	  $this->css="";
		$this->add_css($css);
	}
	public function set_javascript($java) {
	  $this->javascript="";
		$this->add_javascript($java);
	}
	public function set_icon($icon) {
	  $icontype=substr($icon,-3);
		$this->icon="  <link rel=\"icon\" href=\"$icon\" type=\"image/$icontype\">\n";
	}
	public function set_title($title) {
	  $this->title="  <title> $title </title>\n";
	}
	public function set_description($d) {
		$this->description = "  <meta name=\"description\" content=\"$d\">\n";
	}
	public function set_keywords($k) {
		$this->keywords = "  <meta name=\"keywords\" content=\"$k\">\n";
	}

	public function set_custom($string) {
	  $this->custom=$string."\n";
	}

  private function set_default() {
	  $this->set_doctype("strict");
		$this->set_charset("utf8");
		$this->set_title("Kristians hjemmeside");
	}

	public function display() {
	  if (!$this->headertext) $this->makeheader();
		print($this->headertext);
	}

  public function makeheader() {
	  $this->headertext="".
		   $this->doctype."\n".
		   "<html>\n".
			 "<head>\n".
			 $this->title.
			 $this->description.
			 $this->keywords.
			 $this->charset.
			 $this->icon.
			 $this->css.
			 $this->javascript.
			 $this->custom.
			 $this->userdefined.
			 "</head>\n".
			 "";
	}

}

?>
